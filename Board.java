public class Board
{
	//fields
	private Square[][] tictactoeBoard;
	
	//constructor
	public Board()
	{
		this.tictactoeBoard = new Square[3][3];
		for(int i = 0; i < this.tictactoeBoard.length; i++)
		{
			for(int x = 0; x < this.tictactoeBoard[i].length; x++)
			{
				this.tictactoeBoard[i][x]= Square.BLANK;
			}
		}
	}
	
	//override the toString method
	public String toString()
	{
		String returnString = "";
		for( int i = 0; i < this.tictactoeBoard.length; i++)
		{
			for(int y = 0; y < this.tictactoeBoard[i].length; y++)
			{
				returnString += this.tictactoeBoard[i][y] + " ";
			}
			returnString += "\n";
		}
		return returnString;
	}
	
	//QUESTION 4 placeToken method
	public boolean placeToken(int row, int col, Square playerToken)
	{
		int board = tictactoeBoard.length - 1;
		if(row > board || col > board)
		{
			return false;
		}
		
		if(tictactoeBoard[row][col] == Square.BLANK)
		{
		tictactoeBoard[row][col] = playerToken;
            return true;
        }
        else
        {
            return false;
        }
	}

	//Check full
	public boolean checkIfFull()
	{
		for(int i = 0; i < this.tictactoeBoard.length;i++)
		{
			for(int z = 0; z < this.tictactoeBoard[i].length; z++)
			{
				if(this.tictactoeBoard[i][z] == Square.BLANK)
				return false;
			}
		}
		return true;
	}
	
	//QUESTION 6
	private boolean checkIfWinningHorizontal(Square playerToken)
	{
		int counter = 0;
		for(int i = 0; i < this.tictactoeBoard.length; i++)
		{
			for(int h = 0; h < this.tictactoeBoard[i].length; h++)
			{
				if(this.tictactoeBoard[i][h] == playerToken)
					counter++;
				if(counter == tictactoeBoard.length)
					return true;
			}
			counter = 0;
		}
		return false;
	}
	
	//QUESTION 7
	private boolean checkIfWinningVertical(Square playerToken)
	{
		int counter = 0;
		for(int i = 0; i < this.tictactoeBoard.length; i++)
		{
			for(int v = 0; v < this.tictactoeBoard.length; v++)
			{
				if(this.tictactoeBoard[i][v] == playerToken)
					counter++;
				if(counter == tictactoeBoard.length)
					return true;
			}
			counter = 0;
		}
		return false;
	}
	
	//question 8
	public boolean checkIfWinning(Square playerToken)
	{
			boolean victory = checkIfWinningHorizontal(playerToken);
			
			if(victory)
			{
				return true;
			}
			else
			{
				victory = checkIfWinningVertical(playerToken);
				if(victory)
				{
					return true;
				}
			}
			return false;
	}
}
import java.util.Scanner;
public class TicTacToeGame
{
	public static void main (String [] args)
	{
		Scanner scan = new Scanner(System.in);
		
		System.out.println("WElCOME TO TIC-TAC-TOE!!!");
		
		Board board = new Board();
		
		boolean gameOver = false;
		int player = 1;
		Square playToken = Square.X;
		
		 while (!gameOver)
        {
            System.out.println(board);
            if(player == 1)
            {
                playToken = Square.X;
            }
            else
            {
                playToken = Square.O;
            }
			
			//Enter location
			System.out.println("Enter row location.");
			int row = scan.nextInt();
			System.out.println("Enter collumn location.");
			int col = scan.nextInt();
		
			//ReEnter values
			boolean test = false;
			test = board.placeToken(row, col, playToken);
            while (!test)
            {
                System.out.println("Please enter a valid value");
                row = scan.nextInt();
                col = scan.nextInt();
                test = board.placeToken(row, col, playToken);
            }
		
			if (board.checkIfWinning(playToken))
            {
                System.out.println("Player " + player + " has won the game!");
                gameOver = true;
            }
            else if (board.checkIfFull())
            {
                System.out.println("It's a tie!");
                gameOver = true;
            }
            else
            {
                if (player == 1)
                {
                    player++;
                }
                else
                {
                    player = 1;
                }
			}
		}
	}
}